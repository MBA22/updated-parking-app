var Redux = require('redux');
var {loginsignupReducer, feedBackReducer, locationReducer, bookingReducer}=require('./../reducers/index');
var thunk = require('redux-thunk').default;

export var storeConfig = function () {
    var reducer = Redux.combineReducers({

        loginsignupReducer: loginsignupReducer,
        feedBackReducer: feedBackReducer,
        locationReducer: locationReducer,
        bookingReducer: bookingReducer,
    });

    var store = Redux.createStore(reducer, Redux.applyMiddleware(thunk));

    return store;
}