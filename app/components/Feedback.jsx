var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');

var actions = require('./../actions/index');
var Feedback = React.createClass({

    componentDidMount: function () {
        this.props.getFeedback();
    },
    addFeedback: function (e) {
        e.preventDefault(e);
        var feedbackText = this.refs.feedback.value;
        if (feedbackText != null || feedbackText != '')
            this.props.addFeedback(feedbackText);

    },
    render: function () {
        var feedBacks = this.props.feedbacks.feedback;
        console.log("feedbacks", feedBacks);
        if (this.props.feedbacks.feedback == null || this.props.feedbacks.feedback == undefined) {
            return (
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-8 col-sm-offset-2">
                            <h3 className="text-center">Feedback Component</h3>
                            <form onSubmit={this.addFeedback}>
                                <div className="form-group">
                                    <label>Feedback</label>
                                    <input className="form-control" ref="feedback" type="text" placeholder="your feedback"/>
                                </div>
                                <button className="btn btn-success" onClick={this.addFeedback}>Add Feedback</button>
                            </form>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-8 col-sm-offset-2">
                            <h4 className="text-center">FeedBacks:</h4>
                            <p className="text-center">No feedbacks</p>
                        </div>
                    </div>
                </div>
            );
        }
        else return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-8 col-sm-offset-2">
                        <h3 className="text-center">Feedback Component</h3>
                        <form onSubmit={this.addFeedback}>
                            <div className="form-group">
                                <label>Feedback</label>
                                <input className="form-control" ref="feedback" type="text" placeholder="your feedback"/>
                            </div>
                            <button className="btn btn-success" onClick={this.addFeedback}>Add Feedback</button>
                        </form>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-8 col-sm-offset-2">
                        <h4 className="text-center">FeedBacks:</h4>
                        {Object.keys(feedBacks).map(function (key) {
                            return (
                                <blockquote key={key}>
                                    <p>{feedBacks[key].text}</p>
                                    <footer>by: {feedBacks[key].email}</footer>
                                </blockquote>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }
});

function mapStateToProps(state) {

    return {
        feedbacks: state.feedBackReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        addFeedback: actions.addFeedback,
        getFeedback: actions.getFeedback,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Feedback);