var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var UserMain = require('UserMain');
var AdminMain = require('AdminMain');
var Navbar = require('Navbar');
var actions = require('./../actions/index');
var Main = React.createClass({

    componentDidMount: function () {
        this.props.setDate();
    },
    render: function () {
        if (this.props.loginorSignin.role == 'user')
            return (
                <div className="container-fluid">
                    <Navbar></Navbar>
                    <button className="btn btn-danger" onClick={this.props.logout}>Logout</button>
                    <UserMain></UserMain>
                </div>
            );
        else if (this.props.loginorSignin.role == 'admin') {
            return (
                <div className="container-fluid">
                    <Navbar></Navbar>
                    <button className="btn btn-danger" onClick={this.props.logout}>Logout</button>
                    <AdminMain></AdminMain>
                </div>
            );
        }
        else
            return (
                <div className="container-fluid">
                    <h5>Not authorized</h5>
                    <button className="btn btn-danger" onClick={this.props.logout}>Logout</button>
                </div>
            );
    }
});

function mapStateToProps(state) {

    return {
        loginorSignin: state.loginsignupReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        logout: actions.logoutStart,
        setDate: actions.setDate,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Main);