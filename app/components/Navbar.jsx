var React = require("react");
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');
var actions = require('./../actions/index');
var {Link} = require('react-router');

var Navbar = React.createClass({
    render: function () {
        return (

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Parking App</a>
                </div>
                <ul class="nav navbar-nav">
                    <li className="active"><Link to="/main">Home</Link></li>
                    <li><Link to="/feedback">Feedback</Link></li>
                    <li><Link to="/book">Bookings</Link></li>
                </ul>
            </div>
        </nav>
        );
    },
});

module.exports = Navbar;