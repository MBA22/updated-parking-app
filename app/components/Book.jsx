var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');

var actions = require('./../actions/index');
var selectedLocation = '';
var Book = React.createClass({

    componentDidMount: function () {
        this.props.getLocation();
        this.props.getBooking();
    },
    componentDidUpdate: function () {
        var bookings = this.props.bookings.booking;
        if (bookings) {
            Object.keys(bookings).map(function (key) {
                var expireDate = new Date(parseInt(bookings[key].time, 10));
                var currentDate = new Date().getTime();
                var difference = expireDate - currentDate;
                if (difference <= 0) {
                    this.deleteBooking(bookings[key], key)
                }
            }.bind(this))
        }
    },
    bookParking: function (e) {
        e.preventDefault(e);
        var locationKey = this.refs.location.value;
        var currentDate = new Date();
        var time = String(currentDate.setHours(currentDate.getHours() + parseInt(this.refs.time.value, 10)));
        console.log("Location Key for booking", locationKey)
        if (locationKey != null || locationKey != '')
            this.props.addBooking(locationKey, time);

    },
    deleteBooking: function (locationObject, key) {
        this.props.cancelBooking(locationObject, key);
    },
    formatDate: function (ms) {

        var seconds = ms / 1000;
        var hours = parseInt(seconds / 3600);
        seconds = seconds % 3600;
        var minutes = parseInt(seconds / 60);
        seconds = seconds % 60;
        return ( hours + ":" + minutes + ":" + Math.round(seconds));
    },
    render: function () {
        var locations = this.props.locations.location;
        var bookings = this.props.bookings.booking;
        if (this.props.bookings.fetching) {
            return (<p>loading...</p>);
        }
        if (locations == null || locations == undefined) {
            if (bookings == null || bookings == undefined)
                return (
                    <div className="container">
                        <p>No Parking location available</p>
                        <p>No Bookings ye</p>
                    </div>
                );
            else return (
                <div className="container">
                    <div className="row">
                        <div className="col-sm-8 col-sm-offset-2">
                            <p>no location available for parking</p>
                        </div>
                        <div className="row">
                            <div className="col-sm-8 col-sm-offset-2">
                                <h3 className="text-center">Bookings:</h3>

                                {Object.keys(bookings).map(function (key) {
                                    var expireDate = new Date(parseInt(bookings[key].time, 10));
                                    var currentDate = new Date().getTime();
                                    var difference = expireDate - currentDate;
                                    return (
                                        <ul key={key} className="list-group">
                                            <li className=" list-group-item list-group-item-info">{bookings[key].location} </li>
                                            <li className=" list-group-item list-group-item-info">Time
                                                Remaining:{this.formatDate(difference)}</li>
                                            <button className="btn btn-danger"
                                                    onClick={this.deleteBooking.bind(this, bookings[key], key)}>
                                                Cancel
                                                Booking
                                            </button>
                                        </ul>
                                    );

                                }.bind(this))}
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        else if (bookings == null || bookings == undefined) {
            return (
                <div>
                    <h3>Book Parking</h3>
                    <form onSubmit={this.bookParking}>
                        <select className="form-control" ref="location" id="locations">
                            <option value=''>Select location</option>
                            {Object.keys(locations).map(function (key) {
                                return (
                                    <option key={key} value={key}>{locations[key].location}</option>
                                );
                            })}
                        </select>

                        <select className="form-control" ref="time" defaultValue={1}>
                            <option value={1}>1 hour</option>
                            <option value={2}>2 hour</option>
                            <option value={3}>3 hour</option>
                            <option value={4}>4 hour</option>
                        </select>
                        <button className="btn btn-success" onClick={this.bookParking}>Book Parking</button>
                    </form>

                    <h3>Bookings:</h3>
                    <p>No bookings Yet</p>
                </div>

            );
        }
        else
            return (
                <div>
                    <h3>Book Parking</h3>
                    <form onSubmit={this.bookParking}>
                        <select className="form-control" ref="location" id="locations">
                            <option value=''>Select location</option>
                            {Object.keys(locations).map(function (key) {
                                return (
                                    <option key={key} value={key}>{locations[key].location}</option>
                                );
                            })}
                        </select>
                        <select className="form-control" ref="time" defaultValue={1}>
                            <option value={1}>1 hour</option>
                            <option value={2}>2 hour</option>
                            <option value={3}>3 hour</option>
                            <option value={4}>4 hour</option>
                        </select>
                        <button className="btn btn-success" onClick={this.bookParking}>Book Parking</button>

                    </form>

                    <h3>Bookings:</h3>
                    <div>
                        {Object.keys(bookings).map(function (key) {
                            var expireDate = new Date(parseInt(bookings[key].time, 10));
                            var currentDate = new Date(parseInt(this.props.bookings.time, 10));
                            var difference = expireDate - currentDate;
                            return (
                                <ul key={key} className="list-group">
                                    <li className=" list-group-item list-group-item-info">{bookings[key].location} </li>
                                    <li className=" list-group-item list-group-item-info">Time
                                        Remaining:{this.formatDate(difference)}</li>
                                    <button className="btn btn-danger"
                                            onClick={this.deleteBooking.bind(this, bookings[key], key)}>
                                        Cancel
                                        Booking
                                    </button>
                                </ul>
                            );
                        }.bind(this))}
                    </div>
                </div>
            );
    }
});

function mapStateToProps(state) {

    return {
        locations: state.locationReducer,
        bookings: state.bookingReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
            getLocation: actions.getLocation,
            addBooking: actions.addBooking,
            getBooking: actions.getBooking,
            cancelBooking: actions.cancelBooking,
        }, dispatch
    );
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(Book);