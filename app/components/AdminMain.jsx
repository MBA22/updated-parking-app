var React = require('react');
var {connect} = require('react-redux');
var {bindActionCreators}=require('redux');

var actions = require('./../actions/index');
var AdminMain = React.createClass({

    componentDidMount: function () {
        this.props.getLocation();
        this.props.getAllUsers();
    },
    componentDidUpdate: function () {
        var users = this.props.bookings.users;
        if (users) {
            Object.keys(users).map(function (userKeys) {
                var bookings = users[userKeys].bookings;
                return (
                    Object.keys(bookings).map(function (bookingKeys) {
                        var expireDate = new Date(parseInt(bookings[bookingKeys].time, 10));
                        var currentDate = new Date(parseInt(this.props.bookings.time, 10));
                        var difference = expireDate - currentDate;
                        if (difference <= 0) {
                            this.deleteBooking(bookings[bookingKeys], userKeys, bookingKeys)
                        }
                    }.bind(this)));
            }.bind(this))
        }
    },
    addNewLocation: function (e) {
        e.preventDefault(e);
        var location = this.refs.location.value;
        if (location != null || location != '')
            this.props.addLocation(location);
    },
    deleteBooking: function (bookingObject, userKey, bookingKey) {
        this.props.cancelBookingAdmin(bookingObject, userKey, bookingKey);
    },
    formatDate: function (ms) {

        var seconds = ms / 1000;
        var hours = parseInt(seconds / 3600);
        seconds = seconds % 3600;
        var minutes = parseInt(seconds / 60);
        seconds = seconds % 60;
        return ( hours + ":" + minutes + ":" + Math.round(seconds));
    },
    render: function () {
        var _this = this;
        var locations = this.props.locations.location;
        var users = this.props.bookings.users;

        console.log(this.props.bookings.fetching);
        if (this.props.locations.location == null || this.props.locations.location == undefined) {
            if (this.props.bookings.users == null || this.props.bookings.users == undefined) {
                return (
                    <div>
                        <div className="row">
                            <div className="col-sm-8 col-sm-offset-2">
                                <h2 className="text-center">Admin Dashboard</h2>
                                <form onSubmit={this.addNewLocation}>
                                    <div className="form-group">
                                        <label>Location</label>
                                        <input class="form-control" ref="location" type="text" placeholder="Location"/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.addNewLocation}>Add New Location
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-8 col-sm-offset-2">
                                <h3 className="text-center">Available Locations:</h3>
                                <p>no location available</p>
                            </div>
                        </div>
                        <div className="row">
                            <br/>
                            <div className="col-sm-8 col-sm-offset-2">
                                <h3 className="text-center">Bookings:</h3>
                                <p>No bookings</p>
                            </div>
                        </div>
                    </div>
                );
            }
            else {
                return (
                    <div>
                        <div className="row">
                            <div className="col-sm-8 col-sm-offset-2">
                                <h2 className="text-center">Admin Dashboard</h2>
                                <form onSubmit={this.addNewLocation}>
                                    <div className="form-group">
                                        <label>Location</label>
                                        <input class="form-control" ref="location" type="text" placeholder="Location"/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.addNewLocation}>Add New Location
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-8 col-sm-offset-2">
                                <h3 className="text-center">Available Locations:</h3>
                                <p>no location available</p>
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-sm-8 col-sm-offset-2">
                                <h3 className="text-center">Bookings:</h3>
                                {

                                    Object.keys(users).map(function (userKeys) {
                                        var bookings = users[userKeys].bookings;
                                        return (
                                            Object.keys(bookings).map(function (bookingKeys) {
                                                var expireDate = new Date(parseInt(bookings[bookingKeys].time, 10));
                                                var currentDate = new Date(parseInt(this.props.bookings.time, 10));
                                                var difference = expireDate - currentDate;

                                                return (
                                                    <ul className="list-group" key={bookingKeys}>
                                                        <li className=" list-group-item  list-group-item-info">
                                                            Location:{bookings[bookingKeys].location}</li>
                                                        <li className=" list-group-item  list-group-item-info">Time
                                                            Remaining:{this.formatDate(difference)}</li>
                                                        <button className="btn btn-danger"
                                                                onClick={this.deleteBooking.bind(this, bookings[bookingKeys], userKeys, bookingKeys)}>
                                                            Delete
                                                        </button>
                                                    </ul>
                                                );
                                            }.bind(_this))

                                        );
                                    }.bind(_this))
                                }
                            </div>
                        </div>
                    </div>
                );
            }
        }
        else if (this.props.bookings.users == null || this.props.bookings.users == undefined) {
            return (
                <div>
                    <div className="row">
                        <div className="col-sm-8 col-sm-offset-2">
                            <h2 className="text-center">Admin Dashboard</h2>
                            <form onSubmit={this.addNewLocation}>
                                <div className="form-group">
                                    <label>Location</label>
                                    <input class="form-control" ref="location" type="text" placeholder="Location"/>
                                </div>
                                <button className="btn btn-success" onClick={this.addNewLocation}>Add New Location
                                </button>
                            </form>
                        </div>
                    </div>
                    <div className="col-sm-8 col-sm-offset-2">
                        <h3 className="text-center">Available Locations:</h3>

                        {Object.keys(locations).map(function (key) {
                            return (
                                <div className="row">
                                    <ul className="list-group">
                                        <li className=" list-group-item  list-group-item-info" key={key}>
                                            location:{locations[key].location}
                                        </li>
                                    </ul>
                                </div>
                            );
                        })}
                    </div>
                    <br/>
                    <div className="col-sm-8 col-sm-offset-2">
                        <h3 className="text-center">Bookings:</h3>
                        <p>No bookings</p>
                    </div>

                </div>


            );
        }
        else return (
                <div>
                    <div className="row">
                        <div className="col-sm-8 col-sm-offset-2">
                            <h2 className="text-center">Admin Dashboard</h2>
                            <form onSubmit={this.addNewLocation}>
                                <div className="form-group">
                                    <label>Location</label>
                                    <input class="form-control" ref="location" type="text" placeholder="Location"/>
                                </div>
                                <button className="btn btn-success" onClick={this.addNewLocation}>Add New Location
                                </button>
                            </form>
                        </div>
                    </div>

                    <div>
                        <div className="col-sm-8 col-sm-offset-2">
                            <h3 className="text-center">Available Locations:</h3>


                            {Object.keys(locations).map(function (key) {
                                return (
                                    <div className="row">
                                        <ul className="list-group">
                                            <li className=" list-group-item  list-group-item-info" key={key}>
                                                location:{locations[key].location}
                                            </li>
                                        </ul>
                                    </div>
                                );
                            })}
                        </div>
                        <br/>
                        <div className="col-sm-8 col-sm-offset-2">
                            <h3 className="text-center">Bookings:</h3>
                            {

                                Object.keys(users).map(function (userKeys) {
                                    var bookings = users[userKeys].bookings;
                                    return (
                                        Object.keys(bookings).map(function (bookingKeys) {
                                            var expireDate = new Date(parseInt(bookings[bookingKeys].time, 10));
                                            var currentDate = new Date(parseInt(this.props.bookings.time, 10));
                                            var difference = expireDate - currentDate;

                                            return (
                                                <ul className="list-group" key={bookingKeys}>
                                                    <li className=" list-group-item list-group-item-info">
                                                        Location:{bookings[bookingKeys].location}</li>
                                                    <li className=" list-group-item list-group-item-info">Time
                                                        Remaining:{this.formatDate(difference)}</li>
                                                    <button className="btn btn-danger"
                                                            onClick={this.deleteBooking.bind(this, bookings[bookingKeys], userKeys, bookingKeys)}>
                                                        Delete
                                                    </button>
                                                </ul>
                                            );
                                        }.bind(_this))

                                    );
                                }.bind(_this))
                            }
                        </div>
                    </div>
                </div>
            )
                ;
    }
});

function mapStateToProps(state) {

    return {
        locations: state.locationReducer,
        bookings: state.bookingReducer,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        addLocation: actions.addLocation,
        getLocation: actions.getLocation,
        getAllUsers: actions.getAllUsers,
        cancelBookingAdmin: actions.cancelBookingAdmin,
    }, dispatch);
}

module.exports = connect(mapStateToProps, matchDispatchToProps)(AdminMain);